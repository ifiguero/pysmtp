# coding: utf-8
import traceback
import asyncio
import time
import re
import sys
import os
from sqlalchemy.future import select
from .model import db, validate_direccion, get_origen, Direccion, Destinatario, Carta
from .registro import log


class ilabHandler:

	async def handle_HELO(self, server, session, envelope, hostname):
		ip, port = session.peer
		await log.info(u"HELO '{}'".format(hostname))
		
		origen = await get_origen(ip, hostname)

		origen.sesiones = origen.sesiones + 1
			
		await log.info(u"Cliente '{}' en su {} visita".format(hostname, origen.sesiones))
		await db.commit()
		
		session.host_name = 'smtp.vpc.ilab.cl'
		
		return '250 smtp.vpc.ilab.cl'
			
	async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
		await log.info(u"RCPT '{}'".format(address))
		valid = False
		try:
			valid = await validate_direccion(address)
		except BaseException as e:
			await log.error('Traceback {}'.format(traceback.format_exc()))
			
		if valid is None:
			await log.error(u"RCPT ERROR '{}' inválido".format(address))
			return '501 5.5.4 destinatario invalido'
			
		envelope.rcpt_tos.append(address)
		
		return '250 OK'
	
	async def handle_DATA(self, server, session, envelope):
#		await log.debug(u"DATA FROM '{}'".format(envelope.mail_from))
#		await log.debug(u"DATA RCPT TOs '{}'".format(', '.join(envelope.rcpt_tos)))
		# peer = session.peer
		# mail_from = envelope.mail_from
		# rcpt_tos = envelope.rcpt_tos
		# data = envelope.content			# type: bytes
		ip, port = session.peer
		origen = await get_origen(ip, session.host_name)
		
		try:
			dbremitente = await validate_direccion(str(envelope.mail_from))
			dbcarta		= Carta(remitente=dbremitente, contenido=envelope.content.decode('utf-8'))
			db.add(dbcarta)
			await db.commit()
			
			for destinatario in envelope.rcpt_tos:
				dbdestinatario = await validate_direccion(str(destinatario))
				dest = Destinatario(correo=dbdestinatario, carta=dbcarta, enviado=0, intentos=0)
				db.add(dest)
				
			origen.recibidos = origen.recibidos + 1
				
			await db.commit()
			await log.info(u"Correo: '{}'->'{}'".format(envelope.mail_from, ', '.join(envelope.rcpt_tos)))
					
		except:
			await log.error('Traceback {}'.format(traceback.format_exc()))
			return '500 Error interno, reintentelo'
			
		return '250 OK'
