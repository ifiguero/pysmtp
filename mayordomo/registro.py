# coding: utf-8

import os
import asyncio
from aiologger import Logger
from aiologger.formatters.base import Formatter
from aiologger.levels import LogLevel

formato = Formatter(fmt="[%(levelname)s] %(message)s")


if os.environ.get('DEBUG') and (os.environ.get('DEBUG') == 'True' or os.environ.get('DEBUG') == '1' or os.environ.get('DEBUG') == True or os.environ.get('DEBUG') == 1):
	log = Logger.with_default_handlers(name='mayordomo-registro', formatter=formato, level=LogLevel.DEBUG)
#	log.debug('DEBUG enviroment variable is {}'.format(os.environ.get('DEBUG')))
else:
	log = Logger.with_default_handlers(name='mayordomo-registro', formatter=formato, level=LogLevel.INFO)
