from mayordomo import log, pre_process, enviaCorreos, cleanupSent
import os
import daemon
import time
import signal
import asyncio

if not os.environ.get('HEARTBEAT'):
	hb = 60 * 15 # 15 minutos
else:
	hb = int(os.environ.get('HEARTBEAT'))
shouldIrun = True

def main():

	async def main_loop():
		await log.info('Demonio iniciado')
		doki = int(time.time()) + hb
		while shouldIrun:
			if 	await pre_process():
				await enviaCorreos()
			elif await cleanupSent():
				await asyncio.sleep(10)
				
			i = int(time.time())
			if i >= doki:
				doki = i + hb
				await log.info('Heartbeat')
	
	def run():
		signal.signal(signal.SIGTERM, programCleanup)
		asyncio.run(main_loop())
						
	def programCleanup(_signo, _stack_frame):
		log.info('Recibida señal de salida!')
		shouldIrun = False
	
	run()
		
if __name__ == '__main__':
	main()
	