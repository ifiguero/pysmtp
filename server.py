from mayordomo import log, create_async_smtp_server
import os
import daemon
import time
import signal
import asyncio


if not os.environ.get('HEARTBEAT'):
	hb = 60 * 15 # 15 minutos
else:
	hb = int(os.environ.get('HEARTBEAT'))


shouldIrun = True

def main():

	mayordomo = create_async_smtp_server()
	
	async def main_loop():
		await log.info('Demonio iniciado')
		doki=int(time.time()) + hb
		while shouldIrun:
			await asyncio.sleep(1)
			i = int(time.time())
			if i >= doki:
				doki = i + hb
				await log.info('Heartbeat')
	
	def run():
		mayordomo.start()
		signal.signal(signal.SIGTERM, programCleanup)
		asyncio.run(main_loop())
						
	def programCleanup(_signo, _stack_frame):
		log.info('Recibida la señal de salida')
		mayordomo.stop()
		shouldIrun = False
	
	run()
		
if __name__ == '__main__':
	main()
	