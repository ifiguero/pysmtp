#!/bin/bash
cp pysmtp-server.service /etc/systemd/system/pysmtp-server.service
cp pysmtp-sender.service /etc/systemd/system/pysmtp-sender.service

mkdir -p /var/log/pysmtp-sender
mkdir -p /var/log/pysmtp-server

cp -f pysmtp.conf /etc/logrotate.d/pysmtp.conf
chmod 644 /etc/logrotate.d/pysmtp.conf

systemctl unmask pysmtp-server.service
systemctl enable pysmtp-server.service
systemctl start pysmtp-server.service

systemctl unmask pysmtp-sender.service
systemctl enable pysmtp-sender.service
systemctl start pysmtp-sender.service

systemctl restart logrotate
