# pysmtp

Este es un bosquejo de un servidor SMTP Relay. Permite la recepción de correos (Servidor SMTP saliente) y se encarga del envío al destinatario de contactandose con los servidores de Correo del dominio de destino (Servidor MTA).

## Servicios

El software, en la versión actual está compuesto de dos servicios, sincronizados mediante una base de datos:

### Servidor de recepción de correos

Se inicia al ejecutar `server.py` y queda escuchando en el puerto `10025`. Se encarga de recibir los correos e ingresar la "Carta" y las "Direcciones" en la base de datos. También agrega el "FQDN" o dominio del correo en el caso que no se encuentre


### Servidor de envío de correos.

Se inicia al ejecutar `sender.py`. Se conecta a la Base de datos y determina si hay alguna "Carta" pendiente para ser enviada, de ser así verifica que estén actualizadas las direcciones de los servidores MX del dominio del destinatario.

Una vez que todas las direcciones de los servidores están actualizadas, se conecta al servidor "menos utilizado" e intenta entregarla una copia de la carta al destinatario. En caso de ocurrir un error toma el siguiente servidor de la lista y reintenta.

Si ocurren tres intentos fallidos de entrega, se cancela la entrega al destinatario.

## TODO

* Implementación de TLS. Por lo visto requiere certificados válidos y tengo que ver como entregarselos al servidor que sale.
* Mejora de paralelismo asincrónico
* Listas de correo
* Servidor para visualización de historial de estados y estadisticas por servidores y remitentes.